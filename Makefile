CC = gcc
CFLAGS = -ansi -pedantic-errors -Wall -Werror -Wshadow -Wwrite-strings

all: public01.x public02.x public03.x public04.x public05.x public06.x public07.x public08.x public09.x public10.x

public01.x: public01.o graph.o memory-checking.o
	$(CC) $(CFLAGS) public01.o graph.o memory-checking.o -g -o public01.x
public02.x: public02.o graph.o memory-checking.o
	$(CC) $(CFLAGS) public02.o graph.o memory-checking.o -g -o public02.x
public03.x: public03.o graph.o memory-checking.o
	$(CC) $(CFLAGS) public03.o graph.o memory-checking.o -g -o public03.x
public04.x: public04.o graph.o memory-checking.o
	$(CC) $(CFLAGS) public04.o graph.o memory-checking.o -g -o public04.x
public05.x: public05.o graph.o memory-checking.o
	$(CC) $(CFLAGS) public05.o graph.o memory-checking.o -g -o public05.x
public06.x: public06.o graph.o memory-checking.o
	$(CC) $(CFLAGS) public06.o graph.o memory-checking.o -g -o public06.x
public07.x: public07.o graph.o memory-checking.o
	$(CC) $(CFLAGS) public07.o graph.o memory-checking.o -g -o public07.x
public08.x: public08.o graph.o memory-checking.o
	$(CC) $(CFLAGS) public08.o graph.o memory-checking.o -g -o public08.x
public09.x: public09.o graph.o memory-checking.o
	$(CC) $(CFLAGS) public09.o graph.o memory-checking.o -g -o public09.x
public10.x: public10.o graph.o memory-checking.o name-list-to-string.o
	$(CC) $(CFLAGS) public10.o graph.o memory-checking.o name-list-to-string.o -g -o public10.x
public01.o: public01.c
	$(CC) -c $(CFLAGS) public01.c
public02.o: public02.c
	$(CC) -c $(CFLAGS) public02.c
public03.o: public03.c
	$(CC) -c $(CFLAGS) public03.c
public04.o: public04.c
	$(CC) -c $(CFLAGS) public04.c
public05.o: public05.c
	$(CC) -c $(CFLAGS) public05.c
public06.o: public06.c
	$(CC) -c $(CFLAGS) public06.c
public07.o: public07.c
	$(CC) -c $(CFLAGS) public07.c
public08.o: public08.c
	$(CC) -c $(CFLAGS) public08.c
public09.o: public09.c
	$(CC) -c $(CFLAGS) public09.c
public10.o: public10.c 
	$(CC) -c $(CFLAGS) public10.c
graph.o: graph.c graph.h graph-implementation.h
	$(CC) -c $(CFLAGS) graph.c
name-list-to-string.o: name-list-to-string.c name-list-to-string.h
	$(CC) -c $(CFLAGS) name-list-to-string.c
clean:
	rm public*.o graph.o name-list-to-string.o *.x
