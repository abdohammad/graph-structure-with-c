 This project facilitates the creation of a graph and vertices with edges 
  between them, where each edge has a respective cost.
  Each graph has a head pointer to a linked list of vertices
  Each vertex has a name, a link to the next vertex, and a head to a 
  list of edges
  Each Edge has a destination name, lik to the next edge, and a cost 
  A user can retrieve a list of vertices in the graph, and a list 
  of neighbours of each vertex. Any vertex or edge can be removed.