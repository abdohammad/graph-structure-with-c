/*
  Project 5:
  This project facilitates the creation of a graph and vertices with edges 
  between them, where each edge has a respective cost.
  Each graph has a head pointer to a linked list of vertices
  Each vertex has a name, a link to the next vertex, and a head to a 
  list of edges
  Each Edge has a destination name, lik to the next edge, and a cost 
  A user can retrieve a list of vertices in the graph, and a list 
  of neighbours of each vertex. Any vertex or edge can be removed.
  Author: Abdelrahman Hammad    Student ID: 112394561
  Section: 0104                 TA: Mathew
  Directory ID: ahammad 
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "graph.h"
/* Helper function that facilitates recursion to find number of vertices*/
int num_vertices_helper(Vertex *);
/* Helper function that facilitates recursion to add a vertex*/
int add_vertex_helper(Vertex*, const char[]);
/* Helper function that facilitates recursion to check if vertex exists*/
int has_vertex_helper(Vertex*, const char[]);
/* Helper function that facilitates recursion to add edge*/
int add_edge_helper(Vertex *, const char[], const char[],int cost);
/* Helper function that facilitates recursion to add edge */ 
int edge_positioner(Edge *,const char [], int cost);
/* Helper function that facilitates recursion to find edge and return its cost*/
int get_edge_cost_edge_finder(Edge*, const char[]);
/* Helper function that finds a vertex with by its name*/
Vertex* find_vertex(Vertex*, const char[]);
/* Helper function that facilitates recursion to change cost of edge*/ 
int change_edge_cost_helper (Edge*, const char[], int new_cost);
/* Helper function that finds */ 
int num_neighbors_helper(Edge* edge);
/* This function initializes a graph by setting the head of the graph to null */
int compare(const void* a, const void* b);
void clear_vertices(Vertex* vertex);
void clear_edges(Edge* edge);
const char* error = "Error allocating memory";

void init_graph(Graph *graph) {
  graph->head = NULL;
}
/* Function that calls a recursive helper function to calculate the total
number of vertices in the graph*/
int num_vertices(Graph graph) {
  return num_vertices_helper(graph.head);
}
/* Helper funciton that travels through linked list untill it reaches a null
vertex pointer. Each vertex ponter that is not null increases the count by 1*/
int num_vertices_helper(Vertex *vertex) { 
  /* Base case when vertex pointer is null*/
  if(vertex == NULL)
    return 0;
  else
    /* Recursively find the total number of vertices*/
    return 1 + num_vertices_helper(vertex->next);
}
/* Function that adds a vertex. If head of graph is null then this function
adds vertex straight away. Otherwise a helper function is called 
to recursively add a vertex*/
int add_vertex(Graph *graph, const char new_vertex[]) {
  if(graph == NULL || new_vertex == NULL)
    return 0;
  if(graph->head == NULL) {
    /* Allocate memory for vertex pointer*/
    graph->head = (Vertex*) malloc(sizeof(Vertex));
    /*Checks if memory allocation worked*/
    if( graph->head == NULL) {
      printf("%s\n", error);
      exit(1);
    }
    /* Allocates memory for char pointer equal to name being copied*/
    graph->head->name = (char*) malloc(sizeof(char)*(strlen(new_vertex)+1));
    /*Checks if memory allocation worked*/
    if( graph->head->name == NULL) {
      printf("%s\n", error);
      exit(1);
    }
    /*Sets vertex pointer in new vertex to null*/
    graph->head->next = NULL;
    /* Copies the name of the new vertex into the vertex's repsective
       name pointer*/
    strcpy(graph->head->name, new_vertex);
    graph->head->head = NULL;
  }else{
    /* Calls recursive helper function if graph->head is not null*/
    return add_vertex_helper(graph->head, new_vertex);
  }
  return 1;
}
/* Recursive function that adds a vertex to the last vertex in the graph
   its parameters are a verte pointer and the name we want to label this 
   new vertex*/
int add_vertex_helper(Vertex* vertex, const char temp[]){
  /* If the vertex already exists in the graph then return 0*/
  if(strcmp(vertex->name,temp) == 0)
    return 0;
  else
    /* */
    if( vertex->next != NULL ){
      return add_vertex_helper( vertex->next, temp );
      /*Base case below */
    }else {
      /* Allocate memory for vertex pointer*/
      vertex->next = (Vertex*) malloc(sizeof(Vertex));
      /*Checks if memory allocation worked*/
      if( vertex->next == NULL) {
	printf("%s\n", error);
	exit(1);
      }
      /* Allocates memory for char pointer equal to name being copied*/ 
      vertex->next->name = (char*) malloc(sizeof(char)*(strlen(temp)+1));
      /*Checks if memory allocation worked*/
      if(vertex->next->name == NULL) {
	printf("%s\n", error);
	exit(1);
      }
       /* Copies the name of the new vertex into the vertex's repsective
       name pointer*/
      strcpy(vertex->next->name, temp);
       /*Sets vertex pointer in new vertex to null*/
      vertex->next->next = NULL;
      vertex->next->head = NULL;
      return 1;
    }
}
/* Function that calls recursive helper function to check whether a vertex
   is in a graph or not*/
int has_vertex(Graph graph, const char name[]){
  if (name == NULL)
    return 0;
  return has_vertex_helper(graph.head, name);
}
/* Recursive function that traverses through the graph to check whether
   or not the graph contains a vertex with the prameter name*/
int has_vertex_helper(Vertex *vertex , const char temp[]){
  /*Base case 1: if function reaches end of graph return 0, nothing
    was found */
  if(vertex == NULL)
    return 0;
  /*Base case 2: If function finds a vertex with the name being searched
    for, return 1*/
  if(strcmp(vertex->name, temp) == 0){
    return 1;
  }else{
    /*I current vertex is not end of list and if it is not node being 
      searched for then call function for next one*/
    return has_vertex_helper(vertex->next, temp);
  }
}
/* Function that adds an edge to a graph from a vertex to another
   vertex. The funciton uses the string pointer parameter source
   to locate source vertex and string pointer dest to locate destination 
   parameter */
int add_edge(Graph *graph, const char source[], const char dest[],
	     int cost){
  if(graph == NULL || source == NULL || dest == NULL)
    return 0;
  /* Checks validity of inputted parameters, checks whether or not
     cost is negative and whether graph actually contains source and destination
  */
  if(cost < 0 || has_vertex(*graph, source) == 0 ||
     has_vertex(*graph, dest) == 0)
    return 0;
  else
    /* Recursive function that helps add a edge by finding the 
       vertex that is given to the first function*/
    return add_edge_helper(graph->head, source, dest, cost);
}
/* Recursive function that adds will add an edge to a vertex if there
   are no other edges in this vertex, otherwise it calls a recursive
   function that adds edges to the end of the list of edges*/
int add_edge_helper(Vertex *vertex, const char source[], const char dest[],
		    int cost){
  /*Once the vertex is found, function attempts to add edge  */
  if(strcmp(vertex->name, source) == 0){
    /*If no other edge exists in vertex then function adds edge manually by
      allocating memory */
    if(vertex->head == NULL){
      vertex->head = (Edge*) malloc(sizeof(Edge));
      /*Checks if memory allocation worked*/
      if( vertex->head == NULL) {
	printf("%s\n", error);
	exit(1);
      }
    vertex->head->name = (char*) malloc(sizeof(char)*
					strlen(dest)+1);
    /*Checks if memory allocation worked*/
    if( vertex->head->name == NULL) {
	printf("%s\n", error);
	exit(1);
      }
    vertex->head->next = NULL;
    strcpy(vertex->head->name, dest);
    vertex->head->cost = cost;
    return 1;
    }else{
      /* Calls recursive function that traverses edge list
       untill last one is found*/
      return edge_positioner(vertex->head, dest, cost);
    }
  }else{
    return add_edge_helper(vertex->next, source, dest, cost);
  }
}
/* Recursive function that adds edge to end of edge list by finding the 
 the end of the list. Function returns zero if an edge already 
 exists from source to destination.*/
int edge_positioner(Edge *edge,const char dest[], int cost){
  /* Base case 1: If edge already exists then return 0*/
  if(strcmp(edge->name,dest)==0)
    return 0;
  else
    if(edge->next!=NULL){
      return edge_positioner(edge->next, dest, cost);
    }else{
      /* Base case 2: End of edge list reached, then function adds new 
	 edge*/
      edge->next = (Edge*) malloc(sizeof(*(edge->next)));
      /*Checks if memory allocation worked*/
      if( edge->next == NULL) {
	printf("%s\n", error);
	exit(1);
      }
      edge->next->name = (char*) malloc(sizeof(char)*(strlen(dest)+1));
      /*Checks if memory allocation worked*/
      if( edge->next->name == NULL) {
	printf("%s\n", error);
	exit(1);
      }
      strcpy(edge->next->name, dest);
      edge->next->next = NULL;
      edge->next->cost = cost;
      return 1;
    }
}
/* Function that returns the cost of an edge from a vertex source to a vertex
 destination*/
int get_edge_cost(Graph graph, const char source[], const char dest[]){
  /* If source of dest char pointers are null or vertecis do not exist 
     in graph then function retuens -1*/
  if(source == NULL || dest == NULL || has_vertex(graph, source) == 0 ||
     has_vertex(graph, dest) == 0)
    return -1;
  return get_edge_cost_edge_finder((find_vertex((graph.head), source))->head
				   , dest);
}
/* */
int get_edge_cost_edge_finder(Edge* edge, const char dest[]){
  /* Base case 1: Edge between two vertices does not exist*/
  if(edge == NULL)
    return -1;
  else{
    /* Base case 2: Edge between two vertices is found and cost is returned*/
    if(strcmp(edge->name,dest) == 0)
      return edge->cost;
    else
      return get_edge_cost_edge_finder(edge->next, dest);
  } 

}
/* Helper function that recursively tries to find a vertex in a graph
   if ound it will retuen a pointer to that vertex*/
Vertex* find_vertex(Vertex* vertex, const char name[]){
  if(strcmp(vertex->name, name) == 0)
    return vertex;
  else
    return find_vertex(vertex->next, name);
}
/* Function that changes the cost of an edge between two vertices using a 
   recursive helper function*/
int change_edge_cost(Graph *graph, const char source[], const char dest[],
		     int new_cost){
  if(graph == NULL || source == NULL || dest == NULL)
    return 0;
  /* If the new cost is less than zero and if graph does not have 
   source and destination then return 0*/
  if(new_cost < 0 || has_vertex(*graph, source) == 0 ||
     has_vertex(*graph, dest) == 0)
     return 0;
  else
    return change_edge_cost_helper(find_vertex(graph->head, source)->head,
				   dest, new_cost);
}
/* Recursive helper function that helps change cost of vertices*/
int change_edge_cost_helper (Edge* edge, const char dest[], int new_cost){
  /* Base case 1: if edge between source and destination 
     does not exit return 0*/
  if(edge == NULL)
    return 0;
  /* If edge is found then function changes cost of edge*/
  if (strcmp(edge->name,dest)==0){
    edge->cost = new_cost;
    return 1;
  }
  return (change_edge_cost_helper(edge->next, dest, new_cost));

}
/*  Function that uses a recursive  returns the number of edges a vertex has*/
int num_neighbors(Graph graph, const char vertex[]) {
  if( vertex == NULL)
    return -1;
  /* If vertex does not exist in list then function returns -1*/
  if(has_vertex(graph, vertex) == 0)
    return -1;
  return num_neighbors_helper(find_vertex(graph.head,vertex)->head);
}
/* Recursive function that calculates number of edges*/
int num_neighbors_helper(Edge* edge) {
  /* Base case 1: If end of edge list is reached return zero*/
  if(edge == NULL)
    return 0;
  else
    return 1 + num_neighbors_helper(edge->next);
}
/* Function that removes dynamically allocated memory in graph*/
void clear_graph(Graph *graph) {
  if(graph != NULL){
    if(graph->head != NULL)
      clear_vertices(graph->head);
  }
}
/* Recursive function that removes dyamically allocated vertices*/
void clear_vertices(Vertex* vertex) {
  if(vertex->next != NULL)
    clear_vertices(vertex->next);
  if(vertex->head!=NULL)
    clear_edges(vertex->head);
  free(vertex->name);
  vertex->name = NULL;
  free(vertex);
  vertex = NULL;
}
/* Recursive Fuction that removes dynamically allocated graphs*/
void clear_edges(Edge* edge) {
  if(edge != NULL) {
    if(edge->next != NULL)
      clear_edges(edge->next);
    free(edge->name);
    edge->name = NULL;
    free(edge);
    edge = NULL;
  }
}
/*Compare function used by qsort*/
int compare(const void* a, const void * b) {
  return strcmp(*(char **) a, *(char**) b);
}
/*Function that returns list of all vertices*/
char **get_vertices(Graph graph) {
  Vertex* travel;
  int counter = 0;
  char **list_vertices = (char**)malloc(sizeof(char*)*(num_vertices(graph)+1));
  /*Checks if memory allocation worked*/
  if( list_vertices == NULL) {
    printf("%s\n", error);
    exit(1);
  }
  travel = graph.head;
  for(counter = 0; counter < num_vertices(graph); counter ++) {
    list_vertices[counter] = (char*)
      malloc(sizeof(char)*(strlen(travel->name)+1));
    /*Checks if memory allocation worked*/
    if( list_vertices[counter] == NULL) {
      printf("%s\n", error);
      exit(1);
    }
    strcpy(list_vertices[counter],travel->name);
    travel = travel->next;
  }
  travel = NULL;
  list_vertices[counter] = NULL;
  /* Function that sorts list of char pointer*/
  qsort(list_vertices, num_vertices(graph), sizeof(char*), compare);
  return list_vertices;
}
/* Function that frees dynamically allocated memory in a list of char pointers*/
void free_vertex_name_list(char **vertex_names) {
  if (vertex_names != NULL) {
    int counter = 0;
    while(vertex_names[counter] != NULL) {
      free(vertex_names[counter]);
      vertex_names[counter] = NULL;
      counter++;
    }
    free(vertex_names);
    vertex_names = NULL;
  }
}
/*Function gets a list of dynamically allocated list of neighbours*/
char **get_neighbors(Graph graph, const char vertex[]) {
  int counter = 0;
  Vertex* travel;
  Edge* edge_travel;
  char** list_neighbors;
  if(vertex == NULL || !has_vertex(graph,vertex))
    return NULL;
  list_neighbors = (char**)
    malloc(sizeof(char*)*(num_neighbors(graph,vertex)+1));
  /*Checks if memory allocation worked*/
  if(list_neighbors == NULL) {
    printf("%s\n", error);
    exit(1);
  }
  travel = graph.head;
  while(strcmp(travel->name,vertex)!=0)
    travel = travel->next;
  edge_travel = travel->head;
  for(counter = 0; counter < num_neighbors(graph,vertex); counter ++){
    list_neighbors[counter] = (char*)
      malloc(sizeof(char)*(strlen(edge_travel->name)+1));
    /*Checks if memory allocation worked*/
    if(list_neighbors[counter] == NULL) {
      printf("%s\n", error);
      exit(1);
    }
    strcpy(list_neighbors[counter], edge_travel->name);
    edge_travel = edge_travel->next;
  }
  edge_travel = NULL;
  list_neighbors[counter] = NULL;
  /* Sorts the list of pointers*/
  qsort(list_neighbors,num_neighbors(graph,vertex),sizeof(char*),compare);
  return list_neighbors;
}
/* Function that removes an edge from a graph*/
int remove_edge(Graph *graph, const char source[], const char dest[]){
  Vertex* travel;
  Edge* curr;
  Edge* prev;
  if(graph == NULL || source == NULL || dest == NULL
     || !has_vertex(*graph, source) || !has_vertex(*graph, dest))
    return 0;
  
  travel = graph->head;
  /*Find the correct vertex*/
  while(strcmp(travel->name,source) != 0)
    travel = travel->next;
  curr = travel->head;
  prev = NULL;
  while(curr!=NULL && (strcmp(curr->name, dest) != 0)){
    prev = curr;
    curr = curr->next;
  }
  /* If edge does not exist*/
  if(curr == NULL)
    return 0;
  /*First element being removed*/
  if(prev == NULL) {
    travel->head = curr->next;
    free(curr->name);
    curr->name = NULL;
    free(curr);
    curr = NULL;
    return 1;
  }
  /*Non first element being removed*/
  prev->next = curr->next;
  free(curr->name);
  curr->name = NULL;
  free(curr);
  curr = NULL;
  return 1;
}
/*Function that removes vertex from graph*/
int remove_vertex(Graph *graph, const char vertex[]) { 
  Vertex *curr;
  Vertex *prev;
  int counter = 0;
  if(graph == NULL || vertex == NULL|| !has_vertex(*graph, vertex))
    return 0;
  curr = graph->head;
  /*Removes all edges going to vertex*/
  for(counter = 0; counter < num_vertices(*graph); counter ++) {
      remove_edge(graph, curr->name, vertex);
      curr = curr->next;
  }
  curr = graph->head;
  prev = NULL;
  while(strcmp(curr->name, vertex) != 0) {
    prev = curr;
    curr = curr->next;
  }
  /*Clears all the edges going from this vertex*/
  clear_edges(curr->head);
  /*First element being removed*/
  if(prev == NULL) {
    graph->head = curr->next;
    free(curr->name);
    curr->name = NULL;
    free(curr);
    curr = NULL;
    return 1;
  }
  /*Non first element being removed*/
  prev->next = curr->next;
  free(curr->name);
  curr->name = NULL;
  free(curr);
  curr = NULL;
  return 1;
  
}
